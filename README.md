
# Hyphae

Hyphae is a tiny static site generator written in Ruby.
It’s capable of turning a directory tree of markdown files and sub-directories
into a site with a hierarchical navigation menu.

The aim of this project was to create an extremely minimal SSG which was highly tuned to my site-building methodology.
Ordering-specific metadata about pages within the site menu can be encoded with prefixes directly in the page’s filename.
Hyphae also supports creating links to external content in its generated menu with plaintext '.link' files.


## Installing

Hyphae can be installed directly from [Rubygems](https://rubygems.org/gems/hyphae):

    $ gem install hyphae

Alternatively you can install it locally from this repo like so:

    $ git clone https://gitlab.com/henrystanley/hyphae.git
    $ cd hyphae
    $ gem build hyphae.gemspec
    $ gem install hyphae*.gem


## Using

### Site Layout

Hyphae expects an input site directory with the following layout:

    site
      ├── pages
      ├── static
      └── template.html

`pages` is where you'll put the markdown and link files which make up the website.

`static` is for all the site's static files, i.e. CSS, Javascript, images, etc.

`template.html` is the site's main template, more on that later...

Take a look at the included example site directory for a demonstration of this schema.


### Building Your Site

Hyphae is very simple to use. You can invoke it like so:

    $ hyphae

to build a site directory tacitly named `./site` to a build directory `./_build`

You can also explicitly specify the input and output directories:

    $ hyphae -i site_dir -o build_dir

The `--no-file-ext` or simply `-n` can be additionally provided to avoid Hyphae from adding the '.html' suffix to generated page files.


### Pages

Inside your site's `pages` directory you can put Markdown files (`.md`),
link files (`.link`), and subdirectories containing more of the same.

Hyphae will take this tree of nested pages and build a navigation menu out of it:

- Directories are treated as expandable containers for other nodes
- Markdown files are rendered to HTML and represent leaf nodes in the navigation tree
- Link files are expected to contain a valid web-address and are also rendered into leaf nodes which link to the specified address

The filename of a page directory item, minus prefix attributes and file extension, will become the name of the item in the navigation menu.

Several attributes can be added to page directory items by prefixing their filename:

- Prefixing with an underscore, for example `_hidden.md` means that the item/sub-directory will not show up in the menu (though it will still be built)
- Prefixing with 'o' and then a number and then an underscore, for example `o1_first.md`, specifies an absolute order for the item
- Prefixing with 'ut' and then a unix time stamp and an underscore, for example `ut1641028621_Jan1st.md`, specifies a date for the item

In the navigation menu items will be ordered by the following precedence:

    order > date > filename


### Templating

When building your site Hyphae will generate a navigation menu and page content for each page.
The only exception is the index page, which consists only of a navigation menu.

In `template.html` the navigation menu is placed wherever the `{{navbar}}` string occurs.

Likewise, the page content for rendered markdown is placed at the `{{content}}` string.

Additionally, the string `{{index_class}}` is replaced with ` index_page` if the index page is being rendered.
Otherwise this string is removed. This helps with specifying custom CSS only for the index page.
