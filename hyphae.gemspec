Gem::Specification.new do |s|
  s.name = 'hyphae'
  s.version = '0.3.0'
  s.summary = "Hyphae"
  s.description = "A Minimal SSG"
  s.authors = ["Nicholas Rakita"]
  s.email = 'rakita@protonmail.ch'
  s.files = ["lib/hyphae.rb"]
  s.homepage = 'https://rubygems.org/gems/hyphae'
  s.license = 'Unlicense'
  s.add_runtime_dependency 'kramdown', '~> 1.11.1'
  s.executables << 'hyphae'
end
