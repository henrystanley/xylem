require 'kramdown'
require 'fileutils'

module Hyphae

  #########################
  ### Utility Functions ###
  #########################

  # Produces a URL safe version of a string
  def self.sanitize(str)
    str.downcase.gsub(' ', '_').gsub(/[^\d\w_]/, "")
  end

  # Renders an HTML tag
  def self.make_tag(tag, content, attrs={})
    attrs_str = ""
    attrs.map { |x, y| attrs_str << " #{x}=\"#{y}\""}
    "<#{tag}#{attrs_str}>#{content}</#{tag}>"
  end



  #############################
  ### Site Building Classes ###
  #############################

  # Site content node, all other site node classes inherit from this one
  class SiteNode
    attr_reader :name, :slug, :path, :parent, :order, :date

    def initialize(filename, parent=nil)
      @name = File.basename(filename, '.*')
      @parent = parent
      @hidden = false
      @order = nil
      @date = nil
      if captures = @name.match(/^_(.+)/) then
        @hidden = true
        @name = captures[1]
      elsif captures = @name.match(/^o(\d+)_(.+)/) then
        @order = captures[1].to_i
        @name = captures[2]
      elsif captures = @name.match(/^ut(\d+)_(.+)/) then
        @date = Time.at(captures[1].to_i)
        @name = captures[2]
      end
      @slug = Hyphae::sanitize @name
      @path = @parent ? @parent.path + [(@slug)] : []
      return self
    end

    # Get the root SiteNode of a tree of SiteNodes
    def root
      return self if @parent.nil?
      @parent.root
    end

    # Compare this SiteNode with another
    def cmp(other)
      if other.order then
        return 1 if @order.nil?
        return @order <=> other.order
      end
      if other.date then
        return 1 if @date.nil?
        return other.date <=> @date
      end
      @name <=> other.name
    end
  end


  # A SiteDir represents a directory containing nested SiteNodes
  class SiteDir < SiteNode
    def initialize(filename, parent=nil)
      super
      @children = []
      Dir.glob(filename + '/*').each do |f|
        if File.directory? f then
          @children.push SiteDir.new(f, self)
        elsif File.extname(f) == '.md' then
          @children.push SitePage.new(f, self)
        elsif File.extname(f) == '.link' then
          @children.push SiteLink.new(f, self)
        end
      end
      @children.sort! { |x, y| x.cmp(y) }
      return self
    end

    # Returns the html for each child's nav menu link
    def menu_items(open_path=[], options={})
      @children.map { |x| x.nav_link(open_path, options) }.join
    end

    # Returns the html for this SiteDir's nav menu link
    def nav_link(open_path=[], options={})
      return "" if @hidden
      link = Hyphae::make_tag('span', @name, {'class' => 'nav_menu_link'})
      menu_attrs = { 'class' => 'nav_menu_items' }
      menu_attrs['class'] += ' open' if open_path.first == @slug
      menu = Hyphae::make_tag('div', menu_items(open_path[1..-1] || [], options), menu_attrs)
      Hyphae::make_tag('div', link + menu, {'class' => 'nav_menu'})
    end

    # Recursively builds pages for all children
    def build(build_dir, template, options={})
      @children.each { |child| child.build(build_dir, template, options) }
    end

    # Builds an index page for this SiteDir
    def build_index(build_dir, template, index_content='', options={})
      page_html = template.gsub('{{navbar}}', menu_items).gsub("{{content}}", index_content).gsub('{{index_class}}', ' index_page')
      File.open("#{build_dir}/index.html", 'w') { |f| f << page_html }
    end
  end


  # A SitePage represents a markdown document
  class SitePage < SiteNode
    def initialize(filename, parent)
      super
      markdown = File.read filename
      @html = Kramdown::Document.new(markdown).to_html
    end

    # Builds the nav menu tag for this page
    def nav_link(open_path=[], options={})
      return "" if @hidden
      file_ext = options[:no_file_ext] ? '' : '.html'
      attrs = { 'class' => 'nav_page', 'href' => "/#{@path.join('/')}#{file_ext}" }
      attrs['class'] += ' current_page' if @slug == open_path.first
      Hyphae::make_tag('a', @name, attrs)
    end

    # Builds html page
    def build(build_dir, template, options={})
      nav = root.menu_items(@path, options)
      page_html = template.gsub('{{navbar}}', nav).gsub("{{content}}", @html).gsub('{{index_class}}', '')
      page_dir_path = "#{build_dir}/#{@parent.path.join('/')}"
      file_ext = options[:no_file_ext] ? '' : '.html'
      FileUtils.mkdir_p page_dir_path
      File.open("#{page_dir_path}/#{@slug}#{file_ext}", 'w') { |f| f << page_html }
    end
  end


  # A SiteLink represents an arbitrary link
  class SiteLink < SiteNode
    def initialize(filename, parent)
      super
      @link = File.read(filename).strip
    end

    # Builds the nav menu tag for this page
    def nav_link(open_path=[], options={})
      return "" if @hidden
      attrs = { 'class' => 'nav_link', 'href' => @link }
      Hyphae::make_tag('a', @name, attrs)
    end

    # Builds html page (does nothing for a link)
    def build(build_dir, template, options={})
    end
  end

end
